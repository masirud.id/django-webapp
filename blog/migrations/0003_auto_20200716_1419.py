# Generated by Django 3.0.8 on 2020-07-16 14:19

from django.db import migrations
import django_ckeditor_5.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20200716_1334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='body',
            field=django_ckeditor_5.fields.CKEditor5Field(verbose_name='Text'),
        ),
        migrations.AlterField(
            model_name='post',
            name='body',
            field=django_ckeditor_5.fields.CKEditor5Field(verbose_name='Text'),
        ),
    ]
